# Update 12/23/2020

The current version of the project contains an updated Router mlmodel and the scripts to decode the model. This version does a good job in post-processing the data, obtaining bounding boxes and confidence scores while showing them on the camera feed with good confidence scores. You can build the app directly and test it out for yourselves. 

Here is the testing video

![](RouterDetectionLatest.gif)

# Decoding of YOLO v3 Tiny containing Router Auto-tagging module

This project makes use of the .mlmodel(CoreML) obtained from YOLOV3 tiny trained on Gemini and Luna routers using Auto-tagging. This contains the decoded version of mlmodel where post-processing techniques were applied according to the YOLO v3 architecture and based on the output obtained from the .mlmodel that we got from Auto-tagging.

The original source code is here: [YOLO-CoreML-MPSNNGraph](https://github.com/hollance/YOLO-CoreML-MPSNNGraph)
This one uses YOLO v2 model and decodes the mlmodel to obtain bounding boxes and confidence scores. The outputs that this model contains is of shape {125 x 13 x 13}.

There is another version of decoding available that caters to YoloV3 model and the source code is here: [YOLOv3-CoreML](https://github.com/Ma-Dan/YOLOv3-CoreML)
This project uses YOLO v3 model and decodes the mlmodel to obtain bounding boxes and confidence scores. The model used in this project gives out 3 outputs which are of shapes {255, 13, 13}, {255, 26, 26}, {255, 52, 52}. Decoding was done based on this standard shape of the YOLO model.

This is the video of how the original source code works with YOLOv2 model and 20 classes:

![](YOLOv2Decoded.gif)

This is the video of the updated code with Auto-tagging YoloV3 model and 2 classes:

![](AutotaggingModelDecoded.gif)


# My Observations:

1. The shape of the outputs for the models used in the above two versions of the code is of type { boxesPerCell*(numClasses + 5) x xGrids x yGrids }. The decoder code caters to this specific shape of the output. Any other output shape has to further investigated and some more decoding has to be done 
2. The shape that was recieved from the autotagging module is {1 x 2535 x 7} (2535 = 3 * 13 * 13 + 3 * 26 * 26). I wrote a code to divide this into multiple channels and decode them like the way it is done with the above source codes but there seems to be mismatch of the data and the boxes show up all over the place even when the objects are not present in the scene.
